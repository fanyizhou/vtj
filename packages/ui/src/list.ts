import { Plugin } from 'vue';
import {
  XStartup,
  XIcon,
  XMenu,
  XSimpleMask,
  XAction,
  XActionBar,
  XContainer,
  XHeader,
  XPanel,
  XDialog,
  XMask,
  XField
} from './components';

export default [
  XStartup,
  XIcon,
  XMenu,
  XSimpleMask,
  XAction,
  XActionBar,
  XContainer,
  XHeader,
  XPanel,
  XDialog,
  XMask,
  XField
] as unknown as Plugin[];
