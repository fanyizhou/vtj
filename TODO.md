# Engine

- 新建页面、区块校验名称编码，自动转换未大写驼峰格式。
- 新建页面支持设置 keepAlive、权限编码
- 样式设置器
- 代码生成器抽取节点样式放到 style 区块
- 代码生成器 component 特殊元素转换为 HTML 标签
- 代码生成器按需引用图标

# Cli

- 支持自动按需引用 UI、Icons 样式表
- 库模式支持 Tree Shaking

# Serve

- 支持 babel 转换 json 代码块内容

# Utils

- 重构 Request 模块
- 增加日期时间格式化转换函数
- 增加数字格式化转换函数

# UI

- XIcon 图标
- XContainer 布局容器
- XAction 动作
- XActionBar 动作条
- XHeader 头部
- XFooter 底部
- XPanel 面板
- XDialog 弹窗
- XTabs 选项卡
- XMask 母版
- XLogin 登录
- XForm 表单
- XField 表单项
- XTable 表格
- XList 列表

# Docs

- 组件预览

# Adapter
